<?php
/**
 * Muito Além do Bolo - mu-plugin
 *
 * PHP version 7
 *
 * @category  Wordpress_Mu-plugin
 * @package   MuitoAlemdoBolo
 * @author    Cimbre <contato@cimbre.com.br>
 * @copyright 2019 Cimbre
 * @license   Proprietary http://cimbre.com.br
 * @link      https://muitoalemdobolo.com.br
 *
 * @wordpress-plugin
 * Plugin Name: Muito Além do Bolo - mu-plugin
 * Plugin URI:  https://muitoalemdobolo.com.br
 * Description: Customizations for muitoalemdobolo.com.br site
 * Version:     1.0.0
 * Author:      Cimbre
 * Author URI:  http://cimbre.com.br/
 * Text Domain: muitoalemdobolo
 * License:     Proprietary
 * License URI: http://cimbre.com.br
 */

// If this file is called directly, abort.
if (!defined('WPINC')) {
    die;
}

/**
 * Load Translation
 *
 * @return void
 */
add_action(
    'plugins_loaded',
    function () {
        load_muplugin_textdomain('muitoalemdobolocombr', basename(dirname(__FILE__)).'/languages');
    }
);

/**
 * Hide editor from all pages
 */
add_action(
    'admin_init',
    function () {
        remove_post_type_support('page', 'editor');
    }
);

/***********************************************************************************
 * Callback Functions
 **********************************************************************************/

 /**
  * Metabox for Page Slug
  *
  * @param bool  $display  Display
  * @param array $meta_box Metabox 
  *
  * @return bool $display
  *
  * @author Tom Morton
  * @link   https://github.com/CMB2/CMB2/wiki/Adding-your-own-show_on-filters
  */
function Cmb2_Metabox_Show_On_slug($display, $meta_box)
{
    if (!isset($meta_box['show_on']['key'], $meta_box['show_on']['value'])) {
        return $display;
    }

    if ('slug' !== $meta_box['show_on']['key']) {
        return $display;
    }

    $post_id = 0;

    // If we're showing it based on ID, get the current ID
    if (isset($_GET['post'])) {
        $post_id = $_GET['post'];
    } elseif (isset($_POST['post_ID'])) {
        $post_id = $_POST['post_ID'];
    }

    if (!$post_id) {
        return $display;
    }

    $slug = get_post($post_id)->post_name;

    // See if there's a match
    return in_array($slug, (array) $meta_box['show_on']['value']);
}
add_filter('cmb2_show_on', 'Cmb2_Metabox_Show_On_slug', 10, 2);

/**
 * Gets a number of terms and displays them as options
 *
 * @param CMB2_Field $field CMB2 Field
 *
 * @return array An array of options that matches the CMB2 options array
 */
function Cmb2_getTermOptions($field)
{
    $args = $field->args('get_terms_args');
    $args = is_array($args) ? $args : array();

    $args = wp_parse_args($args, array('taxonomy' => 'category'));

    $taxonomy = $args['taxonomy'];

    $terms = (array) cmb2_utils()->wp_at_least('4.5.0')
        ? get_terms($args)
        : get_terms($taxonomy, $args);

    // Initate an empty array
    $term_options = array();
    if (!empty($terms)) {
        foreach ($terms as $term) {
            $term_options[ $term->term_id ] = $term->name;
        }
    }
    return $term_options;
}

/***********************************************************************************
 * Pages Custom Fields
 * ********************************************************************************/

/**
 * Front-page
 */
add_action(
    'cmb2_admin_init',
    function () {
        // Start with an underscore to hide fields from custom fields list
        $prefix = '_muitoalemdobolocombr_frontpage_';

        /**
         * Hero
         */
        $cmb_hero = new_cmb2_box(
            array(
                'id'            => 'muitoalemdobolocombr_frontpage_hero_id',
                'title'         => __('Hero', 'muitoalemdobolocombr'),
                'object_types'  => array('page'), // post type
                'show_on' => array('key' => 'slug', 'value' => 'front-page'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );
        
        //Hero Group
        $hero_id = $cmb_hero->add_field(
            array(
                'id'          => $prefix . 'hero_slides',
                'type'        => 'group',
                'description' => '',
                'options'     => array(
                    'group_title'   =>__('Slide {#}', 'muitoalemdobolocombr'),
                    'add_button'   =>__('Add Another Slide', 'muitoalemdobolocombr'),
                    'remove_button' =>__('Remove Slide', 'muitoalemdobolocombr'),
                    'sortable'      => true, // beta
                ),
            )
        );

        //Background Color
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Background Color', 'muitoalemdobolocombr'),
                'desc'       => '',
                'id'         => 'bkg_color',
                'type'       => 'colorpicker',
                'attributes' => array(
                    'data-colorpicker' => json_encode(
                        array(
                            'palettes' => array('#ffffff', '#e64461', '#fdd200',),
                        )
                    ),
                ),
            )
        );
        
        //Hero Image
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'        => __('Background Image', 'muitoalemdobolocombr'),
                'description' => '',
                'id'          => 'bkg_image',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'muitoalemdobolocombr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                      //'image/gif',
                        'image/jpeg',
                        'image/png',
                    ),
                ),
                'preview_size' => array(320, 180)
            )
        );

        //Hero show gradient
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name' => __('Show gradient below header', 'muitoalemdobolocombr'),
                'desc' => '',
                'id'   => 'show_gradient',
                'type' => 'checkbox',
            )
        );

        //Hero Title
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Title', 'muitoalemdobolocombr'),
                'desc'       => '',
                'id'         => 'title',
                'type'       => 'textarea_code',
            )
        );

        //Hero Text
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Text', 'muitoalemdobolocombr'),
                'desc'       => '',
                'id'         => 'text',
                'type'       => 'textarea_code',
            )
        );

        //Hero Button Text
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Button Text', 'muitoalemdobolocombr'),
                'desc'       => '',
                'id'         => 'btn_text',
                'type'       => 'text_small',
            )
        );

        //Hero Button URL
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Button URL', 'muitoalemdobolocombr'),
                'desc'       => '',
                'id'         => 'btn_url',
                'type'       => 'text',
            )
        );

        //Hero Gradient CSS
        $cmb_hero->add_field(
            array(
                'name'       => __('Gradient CSS', 'muitoalemdobolocombr'),
                'desc'       => '',
                'id'         => $prefix.'hero_css_gradient',
                'type'       => 'textarea_code',
                'default'    => 'linear-gradient(to bottom, rgba(0,0,0,0.25) 0%, rgba(0,0,0,0) 100%);',
            )
        );

        /**
         * Orders
         */
        $cmb_orders = new_cmb2_box(
            array(
                'id'            => 'muitoalemdobolocombr_frontpage_orders_id',
                'title'         => __('Orders', 'muitoalemdobolocombr'),
                'object_types'  => array('page'), // post type
                'show_on' => array('key' => 'slug', 'value' => 'front-page'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );

        //Show Orders
        $cmb_orders->add_field(
            array(
                'name' => __('Show Orders', 'muitoalemdobolocombr'),
                'desc' => __('Show orders on front-page', 'muitoalemdobolocombr'),
                'id'   => $prefix . 'orders_show',
                'type' => 'checkbox',
                'default' => true,
            )
        );

        //Orders Background Color
        $cmb_orders->add_field(
            array(
                'name'       => __('Background Color', 'muitoalemdobolocombr'),
                'desc'       => '',
                'id'         => $prefix . 'orders_background_color',
                'type'       => 'colorpicker',
                'attributes' => array(
                    'data-colorpicker' => json_encode(
                        array(
                            'palettes' => array( '#ffffff', '#e64461', '#fdd200','#fff9f3'),
                        )
                    ),
                ),
            )
        );
        
        //Orders Background Image
        $cmb_orders->add_field(
            array(
                'name'        => __('Background Image', 'muitoalemdobolocombr'),
                'description' => '',
                'id'          => $prefix . 'orders_background_image',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'muitoalemdobolocombr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                        //'image/gif',
                        'image/jpeg',
                        'image/png',
                    ),
                ),
                'preview_size' => array(320, 180)
            )
        );
        
        //Orders Title
        $cmb_orders->add_field(
            array(
                'name'       => __('Title', 'muitoalemdobolocombr'),
                'desc'       => '',
                'id'         => $prefix . 'orders_title',
                'type'       => 'text',
            )
        );

        //Orders Subtitle
        $cmb_orders->add_field(
            array(
                'name'       => __('Subtitle', 'muitoalemdobolocombr'),
                'desc'       => '',
                'id'         => $prefix . 'orders_subtitle',
                'type'       => 'text',
            )
        );

        //Show Orders Categories
        if (class_exists('woocommerce')) {
            $cmb_orders->add_field(
                array(
                    'name'           =>__('Show categories', 'muitoalemdobolocombr'),
                    'desc'           => '',
                    'id'             =>  $prefix . 'orders_show_categories',
                    'type'           => 'multicheck',
                    'options_cb'     => 'Cmb2_getTermOptions',
                    'get_terms_args' => array(
                        'taxonomy'       => 'product_cat',    
                        'orderby' => 'none',
                        'hide_empty' => false,
                        'parent' => 0,
                    ),
                )
            );
        };

        /******
         * Review
         ******/
        $cmb_review = new_cmb2_box(
            array(
                'id'            => 'muitoalemdobolocombr_frontpage_review_id',
                'title'         => __('Review Order', 'muitoalemdobolocombr'),
                'object_types'  => array('page'), // post type
                'show_on' => array('key' => 'slug', 'value' => 'front-page'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );

        //Review Background Color
        $cmb_review->add_field(
            array(
                'name'       => __('Background Color', 'muitoalemdobolocombr'),
                'desc'       => '',
                'id'         => $prefix . 'review_background_color',
                'type'       => 'colorpicker',
                'attributes' => array(
                    'data-colorpicker' => json_encode(
                        array(
                            'palettes' => array( '#ffffff', '#e64461', '#fdd200','#fff9f3'),
                        )
                    ),
                ),
            )
        );
        
        //Review Background Image
        $cmb_review->add_field(
            array(
                'name'        => __('Background Image', 'muitoalemdobolocombr'),
                'description' => '',
                'id'          => $prefix . 'review_background_image',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'muitoalemdobolocombr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                        //'image/gif',
                        'image/jpeg',
                        'image/png',
                    ),
                ),
                'preview_size' => array(320, 180)
            )
        );

        //Review Title
        $cmb_review->add_field(
            array(
                'name'       => __('Title', 'muitoalemdobolocombr'),
                'desc'       => '',
                'id'         => $prefix . 'review_title',
                'type'       => 'textarea_code',
            )
        );

        //Review Subtitle
        $cmb_review->add_field(
            array(
                'name'       => __('Subtitle', 'muitoalemdobolocombr'),
                'desc'       => '',
                'id'         => $prefix . 'review_subtitle',
                'type'       => 'textarea_code',
            )
        );

        //Review Button Text
        $cmb_review->add_field(
            array(
                'name'       => __('Button Text', 'muitoalemdobolocombr'),
                'desc'       => '',
                'default'    => __('Review Order', 'muitoalemdobolocombr'),
                'id'         => $prefix . 'review_btn_text',
                'type'       => 'text_small',
            )
        );

        /******
         * Events
         ******/
        $cmb_events = new_cmb2_box(
            array(
                'id'            => 'muitoalemdobolocombr_frontpage_events_id',
                'title'         => __('Events', 'muitoalemdobolocombr'),
                'object_types'  => array('page'), // post type
                'show_on' => array('key' => 'slug', 'value' => 'front-page'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );

        //Events Background Color
        $cmb_events->add_field(
            array(
                'name'       => __('Background Color', 'muitoalemdobolocombr'),
                'desc'       => '',
                'id'         => $prefix . 'events_background_color',
                'type'       => 'colorpicker',
                'attributes' => array(
                    'data-colorpicker' => json_encode(
                        array(
                            'palettes' => array( '#ffffff', '#e64461', '#fdd200','#fff9f3'),
                        )
                    ),
                ),
            )
        );
        
        //Events Background Image
        $cmb_events->add_field(
            array(
                'name'        => __('Background Image', 'muitoalemdobolocombr'),
                'description' => '',
                'id'          => $prefix . 'events_background_image',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'muitoalemdobolocombr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                        //'image/gif',
                        'image/jpeg',
                        'image/png',
                    ),
                ),
                'preview_size' => array(320, 180)
            )
        );

        //Events Title
        $cmb_events->add_field(
            array(
                'name'       => __('Title', 'muitoalemdobolocombr'),
                'desc'       => '',
                'id'         => $prefix . 'events_title',
                'type'       => 'text',
            )
        );

        //Events Subtitle
        $cmb_events->add_field(
            array(
                'name'       => __('Subtitle', 'muitoalemdobolocombr'),
                'desc'       => '',
                'id'         => $prefix . 'events_subtitle',
                'type'       => 'text',
            )
        );

        //Events Group
        $events_id = $cmb_events->add_field(
            array(
                'id'          => $prefix.'events_items',
                'type'        => 'group',
                'description' => '',
                'options'     => array(
                    'group_title'  =>__('Event type {#}', 'muitoalemdobolocombr'),
                    'add_button' =>__('Add Another Event type', 'muitoalemdobolocombr'),
                    'remove_button'=>__('Remove Event type', 'muitoalemdobolocombr'),
                    'sortable'     => true, // beta
                ),
            )
        );

        //Event Item Icon
        $cmb_events->add_group_field(
            $events_id,
            array(
                'name'    => __('Icon', 'muitoalemdobolocombr'),
                'desc'    => "",
                'id'      => 'icon',
                'type'    => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' => __('Add icon', 'muitoalemdobolocombr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                    //  'image/gif',
                        'image/jpeg',
                        'image/png',
                        'image/svg+xml',
                    ),
                ),
                'preview_size' => array(150, 150)
            )
        );

        //Event Item Title
        $cmb_events->add_group_field(
            $events_id,
            array(
                'name' => __('Title', 'muitoalemdobolocombr'),
                'id'   => 'title',
                'type' => 'textarea_code',
            )
        );

        //Event Item Description
        $cmb_events->add_group_field(
            $events_id,
            array(
            'name' => __('Description', 'muitoalemdobolocombr'),
            'id'   => 'desc',
            'description' => '',
            'type' => 'textarea_code',
            )
        );

        //Event Button Text
        $cmb_events->add_group_field(
            $events_id,
            array(
                'name'       => __('Button Text', 'muitoalemdobolocombr'),
                'desc'       => '',
                'id'         => 'btn_text',
                'type'       => 'text_small',
            )
        );

        //Event Button URL
        $cmb_events->add_group_field(
            $events_id,
            array(
                'name'       => __('Button URL', 'muitoalemdobolocombr'),
                'desc'       => '',
                'id'         => 'btn_url',
                'type'       => 'text',
            )
        );
        
        /**
         * Newsletter
         */
        $cmb_newsletter = new_cmb2_box(
            array(
                'id'            => 'muitoalemdobolocombr_frontpage_newsletter_id',
                'title'         => __('Newsletter CTA', 'muitoalemdobolocombr'),
                'object_types'  => array('page'), // post type
                'show_on' => array('key' => 'slug', 'value' => 'front-page'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );

        //Newsletter Background Color
        $cmb_newsletter->add_field(
            array(
                'name'       => __('Background Color', 'muitoalemdobolocombr'),
                'desc'       => '',
                'id'         => $prefix . 'newsletter_background_color',
                'type'       => 'colorpicker',
                'attributes' => array(
                    'data-colorpicker' => json_encode(
                        array(
                            'palettes' => array( '#ffffff', '#e64461', '#fdd200','#fff9f3'),
                        )
                    ),
                ),
            )
        );
        
        //Newsletter Background Image
        $cmb_newsletter->add_field(
            array(
                'name'        => __('Background Image', 'muitoalemdobolocombr'),
                'description' => '',
                'id'          => $prefix . 'newsletter_background_image',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'muitoalemdobolocombr'),
                ),
                'query_args' => array(
                    'type' => array(
                        //'image/gif',
                        'image/jpeg',
                        'image/png',
                    ),
                ),
                'preview_size' => array(320, 180)
            )
        );

        //Newsletter Title
        $cmb_newsletter->add_field(
            array(
                'name'       => __('Title', 'muitoalemdobolocombr'),
                'desc'       => '',
                'id'         => $prefix . 'newsletter_title',
                'type'       => 'textarea_code',
            )
        );

        //Newsletter Form Shortcode
        $cmb_newsletter->add_field(
            array(
                'name'       => __('Form Shortcode', 'muitoalemdobolocombr'),
                'desc'       => '',
                'id'         => $prefix . 'newsletter_form_shortcode',
                'type'       => 'text',
            )
        );

        /**
         * Contact
         */
        $cmb_contact = new_cmb2_box(
            array(
                'id'            => 'muitoalemdobolo_frontpage_contact_id',
                'title'         => __('Contact', 'muitoalemdobolocombr'),
                'object_types'  => array('page'), // post type
                'show_on' => array('key' => 'slug', 'value' => 'front-page'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );

        //Contact Background Color
        $cmb_contact->add_field(
            array(
                'name'       => __('Background Color', 'muitoalemdobolocombr'),
                'desc'       => '',
                'id'         => $prefix . 'contact_background_color',
                'type'       => 'colorpicker',
                'attributes' => array(
                    'data-colorpicker' => json_encode(
                        array(
                            'palettes' => array( '#ffffff', '#e64461', '#fdd200','#fff9f3'),
                        )
                    ),
                ),
            )
        );
        
        //Contact Background Image
        $cmb_contact->add_field(
            array(
                'name'        => __('Background Image', 'muitoalemdobolocombr'),
                'description' => '',
                'id'          => $prefix . 'contact_background_image',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'muitoalemdobolocombr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                        //'image/gif',
                        'image/jpeg',
                        'image/png',
                    ),
                ),
                'preview_size' => array(320, 180)
            )
        );

        //Stores Group
        $stores_id = $cmb_contact->add_field(
            array(
                'id'          => $prefix.'stores_items',
                'type'        => 'group',
                'description' => '',
                'options'     => array(
                    'group_title'  =>__('Store {#}', 'muitoalemdobolocombr'),
                    'add_button' =>__('Add Another Store', 'muitoalemdobolocombr'),
                    'remove_button'=>__('Remove Store', 'muitoalemdobolocombr'),
                    'sortable'     => true, // beta
                ),
            )
        );

        //Store Item Title
        $cmb_contact->add_group_field(
            $stores_id,
            array(
                'name' => __('Title', 'muitoalemdobolocombr'),
                'id'   => 'title',
                'type' => 'textarea_code',
            )
        );

        //Store Item Description
        $cmb_contact->add_group_field(
            $stores_id,
            array(
            'name' => __('Description', 'muitoalemdobolocombr'),
            'id'   => 'desc',
            'description' => '',
            'type' => 'textarea_code',
            )
        );

        //Store Map URL
        $cmb_contact->add_group_field(
            $stores_id,
            array(
                'name'       => __('Google Maps URL', 'muitoalemdobolocombr'),
                'desc'       => '',
                'id'         => 'map_url',
                'type'       => 'textarea_code',
            )
        );

        /**
         * Footer
         */
        $cmb_footer = new_cmb2_box(
            array(
                'id'            => 'muitoalemdobolo_frontpage_footer_id',
                'title'         => __('Footer', 'muitoalemdobolocombr'),
                'object_types'  => array('page'), // post type
                'show_on' => array('key' => 'slug', 'value' => 'front-page'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );

        //Footer Background Color
        $cmb_footer->add_field(
            array(
                'name'       => __('Background Color', 'muitoalemdobolocombr'),
                'desc'       => '',
                'id'         => $prefix . 'footer_background_color',
                'type'       => 'colorpicker',
                'attributes' => array(
                    'data-colorpicker' => json_encode(
                        array(
                            'palettes' => array( '#ffffff', '#e64461', '#fdd200','#fff9f3'),
                        )
                    ),
                ),
            )
        );
        
        //Footer Background Image
        $cmb_footer->add_field(
            array(
                'name'        => __('Background Image', 'muitoalemdobolocombr'),
                'description' => '',
                'id'          => $prefix . 'footer_background_image',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'muitoalemdobolocombr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                        //'image/gif',
                        'image/jpeg',
                        'image/png',
                    ),
                ),
                'preview_size' => array(320, 180)
            )
        );

        //Footer CNPJ
        $cmb_footer->add_field(
            array(
                'name'       => __('CNPJ', 'muitoalemdobolocombr'),
                'desc'       => '',
                'id'         => $prefix . 'footer_cnpj',
                'type'       => 'text_medium',
            )
        );
    }
);

/**
 * Orders
 */
add_action(
    'cmb2_admin_init',
    function () {
        // Start with an underscore to hide fields from custom fields list
        $prefix = '_muitoalemdobolocombr_orders_';

        /**
         * Hero
         */
        $cmb_hero = new_cmb2_box(
            array(
                'id'            => 'muitoalemdobolocombr_orders_hero_id',
                'title'         => __('Hero', 'muitoalemdobolocombr'),
                'object_types'  => array('page'), // post type
                'show_on' => array('key' => 'slug', 'value' => 'encomendas'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );
        
        //Hero Group
        $hero_id = $cmb_hero->add_field(
            array(
                'id'          => $prefix . 'hero_slides',
                'type'        => 'group',
                'description' => '',
                'options'     => array(
                    'group_title'   =>__('Slide {#}', 'muitoalemdobolocombr'),
                    'add_button'   =>__('Add Another Slide', 'muitoalemdobolocombr'),
                    'remove_button' =>__('Remove Slide', 'muitoalemdobolocombr'),
                    'sortable'      => true, // beta
                ),
            )
        );

        //Background Color
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Background Color', 'muitoalemdobolocombr'),
                'desc'       => '',
                'id'         => 'bkg_color',
                'type'       => 'colorpicker',
                'attributes' => array(
                    'data-colorpicker' => json_encode(
                        array(
                            'palettes' => array('#ffffff', '#e64461', '#fdd200',),
                        )
                    ),
                ),
            )
        );
        
        //Hero Image
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'        => __('Background Image', 'muitoalemdobolocombr'),
                'description' => '',
                'id'          => 'bkg_image',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'muitoalemdobolocombr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                      //'image/gif',
                        'image/jpeg',
                        'image/png',
                    ),
                ),
                'preview_size' => array(320, 180)
            )
        );

        //Hero show gradient
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name' => __('Show gradient below header', 'muitoalemdobolocombr'),
                'desc' => '',
                'id'   => 'show_gradient',
                'type' => 'checkbox',
            )
        );

        //Hero Title
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Title', 'muitoalemdobolocombr'),
                'desc'       => '',
                'id'         => 'title',
                'type'       => 'textarea_code',
            )
        );

        //Hero Text
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Text', 'muitoalemdobolocombr'),
                'desc'       => '',
                'id'         => 'text',
                'type'       => 'textarea_code',
            )
        );

        //Hero Button Text
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Button Text', 'muitoalemdobolocombr'),
                'desc'       => '',
                'id'         => 'btn_text',
                'type'       => 'text_small',
            )
        );

        //Hero Button URL
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Button URL', 'muitoalemdobolocombr'),
                'desc'       => '',
                'id'         => 'btn_url',
                'type'       => 'text',
            )
        );

        //Hero Gradient CSS
        $cmb_hero->add_field(
            array(
                'name'       => __('Gradient CSS', 'muitoalemdobolocombr'),
                'desc'       => '',
                'id'         => $prefix.'hero_css_gradient',
                'type'       => 'textarea_code',
                'default'    => 'linear-gradient(to bottom, rgba(0,0,0,0.25) 0%, rgba(0,0,0,0) 100%);',
            )
        );

        /**
         * Content
         */
        $cmb_content = new_cmb2_box(
            array(
                'id'            => 'muitoalemdobolocombr_orders_content_id',
                'title'         => __('Content', 'muitoalemdobolocombr'),
                'object_types'  => array('page'), // post type
                'show_on' => array('key' => 'slug', 'value' => 'encomendas'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );

        //Orders Background Color
        $cmb_content->add_field(
            array(
                'name'       => __('Background Color', 'muitoalemdobolocombr'),
                'desc'       => '',
                'id'         => $prefix . 'content_background_color',
                'type'       => 'colorpicker',
                'attributes' => array(
                    'data-colorpicker' => json_encode(
                        array(
                            'palettes' => array( '#ffffff', '#e64461', '#fdd200','#fff9f3'),
                        )
                    ),
                ),
            )
        );
        
        //Orders Background Image
        $cmb_content->add_field(
            array(
                'name'        => __('Background Image', 'muitoalemdobolocombr'),
                'description' => '',
                'id'          => $prefix . 'content_background_image',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'muitoalemdobolocombr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                        //'image/gif',
                        'image/jpeg',
                        'image/png',
                    ),
                ),
                'preview_size' => array(320, 180)
            )
        );
        
        //Orders Title
        $cmb_content->add_field(
            array(
                'name'       => __('Title', 'muitoalemdobolocombr'),
                'desc'       => '',
                'id'         => $prefix . 'content_title',
                'type'       => 'text',
            )
        );

        //Orders Subtitle
        $cmb_content->add_field(
            array(
                'name'       => __('Subtitle', 'muitoalemdobolocombr'),
                'desc'       => '',
                'id'         => $prefix . 'content_subtitle',
                'type'       => 'text',
            )
        );

        //Show Orders Categories
        if (class_exists('woocommerce')) {
            $cmb_content->add_field(
                array(
                    'name'           =>__('Show categories', 'muitoalemdobolocombr'),
                    'desc'           => '',
                    'id'             =>  $prefix . 'content_show_categories',
                    'type'           => 'multicheck',
                    'options_cb'     => 'Cmb2_getTermOptions',
                    'get_terms_args' => array(
                        'taxonomy'       => 'product_cat',    
                        'orderby' => 'none',
                        'hide_empty' => false,
                        'parent' => 0,
                    ),
                )
            );
        };

        /******
         * Review
         ******/
        $cmb_review = new_cmb2_box(
            array(
                'id'            => 'muitoalemdobolocombr_orders_review_id',
                'title'         => __('Review Order', 'muitoalemdobolocombr'),
                'object_types'  => array('page'), // post type
                'show_on' => array('key' => 'slug', 'value' => 'encomendas'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );

        //Review Background Color
        $cmb_review->add_field(
            array(
                'name'       => __('Background Color', 'muitoalemdobolocombr'),
                'desc'       => '',
                'id'         => $prefix . 'review_background_color',
                'type'       => 'colorpicker',
                'attributes' => array(
                    'data-colorpicker' => json_encode(
                        array(
                            'palettes' => array( '#ffffff', '#e64461', '#fdd200','#fff9f3'),
                        )
                    ),
                ),
            )
        );
        
        //Review Background Image
        $cmb_review->add_field(
            array(
                'name'        => __('Background Image', 'muitoalemdobolocombr'),
                'description' => '',
                'id'          => $prefix . 'review_background_image',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'muitoalemdobolocombr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                        //'image/gif',
                        'image/jpeg',
                        'image/png',
                    ),
                ),
                'preview_size' => array(320, 180)
            )
        );

        //Review Title
        $cmb_review->add_field(
            array(
                'name'       => __('Title', 'muitoalemdobolocombr'),
                'desc'       => '',
                'id'         => $prefix . 'review_title',
                'type'       => 'textarea_code',
            )
        );

        //Review Subtitle
        $cmb_review->add_field(
            array(
                'name'       => __('Subtitle', 'muitoalemdobolocombr'),
                'desc'       => '',
                'id'         => $prefix . 'review_subtitle',
                'type'       => 'textarea_code',
            )
        );

        //Review Button Text
        $cmb_review->add_field(
            array(
                'name'       => __('Button Text', 'muitoalemdobolocombr'),
                'desc'       => '',
                'default'    => __('Review Order', 'muitoalemdobolocombr'),
                'id'         => $prefix . 'review_btn_text',
                'type'       => 'text_small',
            )
        );

    }
);

/**
 * About
 */
add_action(
    'cmb2_admin_init',
    function () {
        // Start with an underscore to hide fields from custom fields list
        $prefix = '_muitoalemdobolocombr_about_';

        /**
         * Hero
         */
        $cmb_hero = new_cmb2_box(
            array(
                'id'            => 'muitoalemdobolocombr_about_hero_id',
                'title'         => __('Hero', 'muitoalemdobolocombr'),
                'object_types'  => array('page'), // post type
                'show_on' => array('key' => 'slug', 'value' => 'institucional'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );
        
        //Hero Group
        $hero_id = $cmb_hero->add_field(
            array(
                'id'          => $prefix . 'hero_slides',
                'type'        => 'group',
                'description' => '',
                'options'     => array(
                    'group_title'   => __('Slide {#}', 'muitoalemdobolocombr'),
                    'add_button'  => __('Add Another Slide', 'muitoalemdobolocombr'),
                    'remove_button' => __('Remove Slide', 'muitoalemdobolocombr'),
                    'sortable'      => true, // beta
                ),
            )
        );

        //Background Color
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Background Color', 'muitoalemdobolocombr'),
                'desc'       => '',
                'id'         => 'bkg_color',
                'type'       => 'colorpicker',
                'attributes' => array(
                    'data-colorpicker' => json_encode(
                        array(
                            'palettes' => array( '#ffffff', '#e64461', '#fdd200','#fff9f3'),
                        )
                    ),
                ),
            )
        );
        
        //Hero Image
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'        => __('Background Image', 'muitoalemdobolocombr'),
                'description' => '',
                'id'          => 'bkg_image',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'muitoalemdobolocombr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                      //'image/gif',
                        'image/jpeg',
                        'image/png',
                    ),
                ),
                'preview_size' => array(320, 180)
            )
        );

        //Hero show gradient
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name' => __('Show gradient below header', 'muitoalemdobolocombr'),
                'desc' => '',
                'id'   => 'show_gradient',
                'type' => 'checkbox',
            )
        );

        //Hero Title
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Title', 'muitoalemdobolocombr'),
                'desc'       => '',
                'id'         => 'title',
                'type'       => 'textarea_code',
            )
        );

        //Hero Text
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Text', 'muitoalemdobolocombr'),
                'desc'       => '',
                'id'         => 'text',
                'type'       => 'textarea_code',
            )
        );

        //Hero Gradient CSS
        $cmb_hero->add_field(
            array(
                'name'       => __('Gradient CSS', 'muitoalemdobolocombr'),
                'desc'       => '',
                'id'         => $prefix.'hero_css_gradient',
                'type'       => 'textarea_code',
                'default'    => 'linear-gradient(to bottom, rgba(0,0,0,0.25) 0%, rgba(0,0,0,0) 100%);',
            )
        );

        /**
         * MADB
         */
        $cmb_madb = new_cmb2_box(
            array(
                'id'            => 'muitoalemdobolocombr_about_madb_id',
                'title'         => __('Muito Além do Bolo', 'muitoalemdobolocombr'),
                'object_types'  => array('page'), // post type
                'show_on' => array('key' => 'slug', 'value' => 'institucional'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );

        //Background Color
        $cmb_madb->add_field(
            array(
                'name'       => __('Background Color', 'muitoalemdobolocombr'),
                'desc'       => '',
                'id'         => $prefix . 'madb_background_color',
                'type'       => 'colorpicker',
                'attributes' => array(
                    'data-colorpicker' => json_encode(
                        array(
                            'palettes' => array('#ffffff', '#e64461', '#fdd200',),
                        )
                    ),
                ),
            )
        );

        //Background Image
        $cmb_madb->add_field(
            array(
                'name'        => __('Background Image', 'muitoalemdobolocombr'),
                'description' => '',
                'id'          => $prefix . 'madb_background_image',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'muitoalemdobolocombr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                        //'image/gif',
                        'image/jpeg',
                        'image/png',
                    ),
                ),
                'preview_size' => array(320, 180)
            )
        );

        //Title
        $cmb_madb->add_field(
            array(
                'name'       => __('Title', 'muitoalemdobolocombr'),
                'desc'       => '',
                'id'         => $prefix . 'madb_title',
                'type'       => 'textarea_code',
            )
        );

        //Subtitle
        $cmb_madb->add_field(
            array(
                'name'       => __('Subtitle', 'muitoalemdobolocombr'),
                'desc'       => '',
                'id'         => $prefix . 'madb_subtitle',
                'type'       => 'textarea_code',
            )
        );

        //Content Text
        $cmb_madb->add_field(
            array(
                'name'       => __('Content Text', 'muitoalemdobolocombr'),
                'desc'       => '',
                'id'         => $prefix . 'madb_content_text',
                'type'       => 'textarea_code',
            )
        );

        /**
         * Bruna
         */
        $cmb_bruna = new_cmb2_box(
            array(
                'id'            => 'muitoalemdobolocombr_about_bruna_id',
                'title'         => __('Muito Além do Bolo', 'muitoalemdobolocombr'),
                'object_types'  => array('page'), // post type
                'show_on' => array('key' => 'slug', 'value' => 'institucional'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );

        //Background Color
        $cmb_bruna->add_field(
            array(
                'name'       => __('Background Color', 'muitoalemdobolocombr'),
                'desc'       => '',
                'id'         => $prefix . 'bruna_background_color',
                'type'       => 'colorpicker',
                'attributes' => array(
                    'data-colorpicker' => json_encode(
                        array(
                            'palettes' => array( '#ffffff', '#e64461', '#fdd200','#fff9f3'),
                        )
                    ),
                ),
            )
        );

        //Background Image
        $cmb_bruna->add_field(
            array(
                'name'        => __('Background Image', 'muitoalemdobolocombr'),
                'description' => '',
                'id'          => $prefix . 'bruna_background_image',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'muitoalemdobolocombr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                        //'image/gif',
                        'image/jpeg',
                        'image/png',
                    ),
                ),
                'preview_size' => array(320, 180)
            )
        );
        
        //Title
        $cmb_bruna->add_field(
            array(
                'name'       => __('Title', 'muitoalemdobolocombr'),
                'desc'       => '',
                'id'         => $prefix . 'bruna_title',
                'type'       => 'textarea_code',
            )
        );

        //Subtitle
        $cmb_bruna->add_field(
            array(
                'name'       => __('Subtitle', 'muitoalemdobolocombr'),
                'desc'       => '',
                'id'         => $prefix . 'bruna_subtitle',
                'type'       => 'textarea_code',
            )
        );

        //Featured Image
        $cmb_bruna->add_field(
            array(
                'name'        => __('Featured Image', 'muitoalemdobolocombr'),
                'description' => '',
                'id'          => $prefix . 'bruna_featured_image',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'muitoalemdobolocombr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                        //'image/gif',
                        'image/jpeg',
                        'image/png',
                    ),
                ),
                'preview_size' => array(320, 180)
            )
        );

        //Content Text
        $cmb_bruna->add_field(
            array(
                'name'       => __('Content Text', 'muitoalemdobolocombr'),
                'desc'       => '',
                'id'         => $prefix . 'bruna_content_text',
                'type'       => 'textarea_code',
            )
        );

        /******
         * CTA
         ******/
        $cmb_cta = new_cmb2_box(
            array(
                'id'            => 'muitoalemdobolocombr_about_cta_id',
                'title'         => __('cta', 'muitoalemdobolocombr'),
                'object_types'  => array('page'), // post type
                'show_on' => array('key' => 'slug', 'value' => 'institucional'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );

        //Background Color
        $cmb_cta->add_field(
            array(
                'name'       => __('Background Color', 'muitoalemdobolocombr'),
                'desc'       => '',
                'id'         => $prefix . 'cta_background_color',
                'type'       => 'colorpicker',
                'attributes' => array(
                    'data-colorpicker' => json_encode(
                        array(
                            'palettes' => array( '#ffffff', '#e64461', '#fdd200','#fff9f3'),
                        )
                    ),
                ),
            )
        );

        //Background Image
        $cmb_cta->add_field(
            array(
                'name'        => __('Background Image', 'muitoalemdobolocombr'),
                'description' => '',
                'id'          => $prefix . 'cta_background_image',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'muitoalemdobolocombr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                        //'image/gif',
                        'image/jpeg',
                        'image/png',
                    ),
                ),
                'preview_size' => array(320, 180)
            )
        );
        
        //Quote
        $cmb_cta->add_field(
            array(
                'name'       => __('Quote', 'muitoalemdobolocombr'),
                'desc'       => '',
                'id'         => $prefix . 'cta_quote',
                'type'       => 'textarea_code',
            )
        );

        //Author
        $cmb_cta->add_field(
            array(
                'name'       => __('Author', 'muitoalemdobolocombr'),
                'desc'       => '',
                'id'         => $prefix . 'cta_author',
                'type'       => 'text',
            )
        );

        /**
         * Gallery
         */
        $cmb_gallery = new_cmb2_box(
            array(
                'id'            => 'muitoalemdobolocombr_about_gallery_id',
                'title'         => __('Gallery', 'muitoalemdobolocombr'),
                'object_types'  => array('page'), // post type
                'show_on' => array('key' => 'slug', 'value' => 'institucional'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );

        //Gallery Images
        $cmb_gallery->add_field(
            array(
                'name' => __('Gallery images', 'muitoalemdobolocombr'),
                'id'   =>  $prefix . 'gallery_images',
                'type' => 'file_list',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text' => array(
                    'add_upload_files_text' => __('Add Images', 'muitoalemdobolocombr'),
                    'remove_image_text' => __('Remove Image', 'muitoalemdobolocombr'),
                    'file_text' => __('File', 'muitoalemdobolocombr'),
                    'file_download_text' => __('Download', 'muitoalemdobolocombr'),
                    'remove_text' => __('Remove', 'muitoalemdobolocombr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                    //  'image/gif',
                        'image/jpeg',
                        'image/png',
                        ),
                ),
                'preview_size' => array(320, 180)
            )
        );
    }
);

/**
 * Contact
 */
add_action(
    'cmb2_admin_init',
    function () {
        // Start with an underscore to hide fields from custom fields list
        $prefix = '_muitoalemdobolocombr_contact_';

        /**
         * Hero
         */
        $cmb_hero = new_cmb2_box(
            array(
                'id'            => 'muitoalemdobolocombr_contact_hero_id',
                'title'         => __('Hero', 'muitoalemdobolocombr'),
                'object_types'  => array('page'), // post type
                'show_on' => array('key' => 'slug', 'value' => 'contato'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );
        
        //Hero Group
        $hero_id = $cmb_hero->add_field(
            array(
                'id'          => $prefix . 'hero_slides',
                'type'        => 'group',
                'description' => '',
                'options'     => array(
                    'group_title'   => __('Slide {#}', 'muitoalemdobolocombr'),
                    'add_button'  => __('Add Another Slide', 'muitoalemdobolocombr'),
                    'remove_button' => __('Remove Slide', 'muitoalemdobolocombr'),
                    'sortable'      => true, // beta
                ),
            )
        );
        
        //Background Color
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Background Color', 'muitoalemdobolocombr'),
                'desc'       => '',
                'id'         => 'bkg_color',
                'type'       => 'colorpicker',
                'attributes' => array(
                    'data-colorpicker' => json_encode(
                        array(
                            'palettes' => array( '#ffffff', '#e64461', '#fdd200','#fff9f3'),
                        )
                    ),
                ),
            )
        );
                
        //Hero Image
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'        => __('Background Image', 'muitoalemdobolocombr'),
                'description' => '',
                'id'          => 'bkg_image',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'muitoalemdobolocombr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                      //'image/gif',
                        'image/jpeg',
                        'image/png',
                    ),
                ),
                'preview_size' => array(320, 180)
            )
        );

        //Hero show gradient
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name' => __('Show gradient below header', 'muitoalemdobolocombr'),
                'desc' => '',
                'id'   => 'show_gradient',
                'type' => 'checkbox',
            )
        );

        //Hero Title
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Title', 'muitoalemdobolocombr'),
                'desc'       => '',
                'id'         => 'title',
                'type'       => 'textarea_code',
            )
        );

        //Hero Text
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Text', 'muitoalemdobolocombr'),
                'desc'       => '',
                'id'         => 'text',
                'type'       => 'textarea_code',
            )
        );

        //Hero Gradient CSS
        $cmb_hero->add_field(
            array(
                'name'       => __('Gradient CSS', 'muitoalemdobolocombr'),
                'desc'       => '',
                'id'         => $prefix . 'hero_css_gradient',
                'type'       => 'textarea_code',
                'default'    => 'linear-gradient(to bottom, rgba(0,0,0,0.25) 0%, rgba(0,0,0,0) 100%);',
            )
        );

        /**
         * Form
         */
        $cmb_form = new_cmb2_box(
            array(
                'id'            => 'muitoalemdobolocombr_contact_form_id',
                'title'         => __('Contact Form', 'muitoalemdobolocombr'),
                'object_types'  => array('page'), // post type
                'show_on' => array('key' => 'slug', 'value' => 'contato'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );

        //Background Color
        $cmb_form->add_field(
            array(
                'name'       => __('Background Color', 'muitoalemdobolocombr'),
                'desc'       => '',
                'id'         => $prefix . 'form_background_color',
                'type'       => 'colorpicker',
                'attributes' => array(
                    'data-colorpicker' => json_encode(
                        array(
                            'palettes' => array( '#ffffff', '#e64461', '#fdd200','#fff9f3'),
                        )
                    ),
                ),
            )
        );

        //Background Image
        $cmb_form->add_field(
            array(
                'name'        => __('Background Image', 'muitoalemdobolocombr'),
                'description' => '',
                'id'          => $prefix . 'form_background_image',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'muitoalemdobolocombr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                        //'image/gif',
                        'image/jpeg',
                        'image/png',
                    ),
                ),
                'preview_size' => array(320, 180)
            )
        );

        //Title
        $cmb_form->add_field(
            array(
                'name'       => __('Title', 'muitoalemdobolocombr'),
                'desc'       => '',
                'id'         => $prefix . 'form_title',
                'type'       => 'text',
            )
        );

        //Subtitle
        $cmb_form->add_field(
            array(
                'name'       => __('Subtitle', 'muitoalemdobolocombr'),
                'desc'       => '',
                'id'         => $prefix . 'form_subtitle',
                'type'       => 'text',
            )
        );

        //Shortcode
        $cmb_form->add_field(
            array(
                'name'       => __('Form Shortcode', 'muitoalemdobolocombr'),
                'desc'       => '',
                'id'         => $prefix . 'form_shortcode',
                'type'       => 'textarea_code',
            )
        );
    }
);

/**
 * Events
 */
add_action(
    'cmb2_admin_init',
    function () {
        // Start with an underscore to hide fields from custom fields list
        $prefix = '_muitoalemdobolocombr_events_';

        /**
         * Hero
         */
        $cmb_hero = new_cmb2_box(
            array(
                'id'            => 'muitoalemdobolocombr_events_hero_id',
                'title'         => __('Hero', 'muitoalemdobolocombr'),
                'object_types'  => array('page'), // post type
                'show_on' => array('key' => 'page-template', 'value' => 'views/template-eventos.blade.php'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );
        
        //Hero Group
        $hero_id = $cmb_hero->add_field(
            array(
                'id'          => $prefix . 'hero_slides',
                'type'        => 'group',
                'description' => '',
                'options'     => array(
                    'group_title'   => __('Slide {#}', 'muitoalemdobolocombr'),
                    'add_button'  => __('Add Another Slide', 'muitoalemdobolocombr'),
                    'remove_button' => __('Remove Slide', 'muitoalemdobolocombr'),
                    'sortable'      => true, // beta
                ),
            )
        );

        //Background Color
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Background Color', 'muitoalemdobolocombr'),
                'desc'       => '',
                'id'         => 'bkg_color',
                'type'       => 'colorpicker',
                'attributes' => array(
                    'data-colorpicker' => json_encode(
                        array(
                            'palettes' => array( '#ffffff', '#e64461', '#fdd200','#fff9f3'),
                        )
                    ),
                ),
            )
        );
        
        //Hero Image
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'        => __('Background Image', 'muitoalemdobolocombr'),
                'description' => '',
                'id'          => 'bkg_image',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'muitoalemdobolocombr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                      //'image/gif',
                        'image/jpeg',
                        'image/png',
                    ),
                ),
                'preview_size' => array(320, 180)
            )
        );

        //Hero show gradient
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name' => __('Show gradient below header', 'muitoalemdobolocombr'),
                'desc' => '',
                'default' => true,
                'id'   => 'show_gradient',
                'type' => 'checkbox',
            )
        );

        //Hero Title
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Title', 'muitoalemdobolocombr'),
                'desc'       => '',
                'id'         => 'title',
                'type'       => 'textarea_code',
            )
        );

        //Hero Text
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Text', 'muitoalemdobolocombr'),
                'desc'       => '',
                'id'         => 'text',
                'type'       => 'textarea_code',
            )
        );

        //Hero Gradient CSS
        $cmb_hero->add_field(
            array(
                'name'       => __('Gradient CSS', 'muitoalemdobolocombr'),
                'desc'       => '',
                'id'         => $prefix.'hero_css_gradient',
                'type'       => 'textarea_code',
                'default'    => 'linear-gradient(to bottom, rgba(0,0,0,0.25) 0%, rgba(0,0,0,0) 100%);',
            )
        );

        /******
         * Events
         ******/
        $cmb_events = new_cmb2_box(
            array(
                'id'            => 'muitoalemdobolocombr_events_content_id',
                'title'         => __('Events', 'muitoalemdobolocombr'),
                'object_types'  => array('page'), // post type
                'show_on' => array('key' => 'page-template', 'value' => 'views/template-eventos.blade.php'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );

        //Events Background Color
        $cmb_events->add_field(
            array(
                'name'       => __('Background Color', 'muitoalemdobolocombr'),
                'desc'       => '',
                'id'         => $prefix . 'content_background_color',
                'type'       => 'colorpicker',
                'attributes' => array(
                    'data-colorpicker' => json_encode(
                        array(
                            'palettes' => array( '#ffffff', '#e64461', '#fdd200','#fff9f3'),
                        )
                    ),
                ),
            )
        );
        
        //Events Background Image
        $cmb_events->add_field(
            array(
                'name'        => __('Background Image', 'muitoalemdobolocombr'),
                'description' => '',
                'id'          => $prefix . 'content_background_image',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'muitoalemdobolocombr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                        //'image/gif',
                        'image/jpeg',
                        'image/png',
                    ),
                ),
                'preview_size' => array(320, 180)
            )
        );

        //Events Title
        $cmb_events->add_field(
            array(
                'name'       => __('Title', 'muitoalemdobolocombr'),
                'desc'       => '',
                'id'         => $prefix . 'content_title',
                'type'       => 'text',
            )
        );

        //Events Subtitle
        $cmb_events->add_field(
            array(
                'name'       => __('Subtitle', 'muitoalemdobolocombr'),
                'desc'       => '',
                'id'         => $prefix . 'content_subtitle',
                'type'       => 'text',
            )
        );

        //Events Group
        $events_id = $cmb_events->add_field(
            array(
                'id'          => $prefix.'content_items',
                'type'        => 'group',
                'description' => '',
                'options'     => array(
                    'group_title'   => __('Event type {#}', 'muitoalemdobolocombr'),
                    'add_button'    => __('Add Another Event type', 'muitoalemdobolocombr'),
                    'remove_button' => __('Remove Event type', 'muitoalemdobolocombr'),
                    'sortable'      => true, // beta
                ),
            )
        );

        //Event Item Icon
        $cmb_events->add_group_field(
            $events_id,
            array(
                'name'    => __('Icon', 'muitoalemdobolocombr'),
                'desc'    => "",
                'id'      => 'icon',
                'type'    => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' => __('Add icon', 'muitoalemdobolocombr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                        //  'image/gif',
                        'image/jpeg',
                        'image/png',
                        'image/svg+xml',
                    ),
                ),
                'preview_size' => array(150, 150)
            )
        );

        //Event Item Title
        $cmb_events->add_group_field(
            $events_id,
            array(
                'name' => __('Title', 'muitoalemdobolocombr'),
                'id'   => 'title',
                'type' => 'textarea_code',
            )
        );

        //Event Item Description
        $cmb_events->add_group_field(
            $events_id,
            array(
            'name' => __('Description', 'muitoalemdobolocombr'),
            'id'   => 'desc',
            'description' => '',
            'type' => 'textarea_code',
            )
        );

        /**
         * Gallery
         */
        $cmb_gallery = new_cmb2_box(
            array(
                'id'            => 'muitoalemdobolocombr_events_gallery_id',
                'title'         => __('Gallery', 'muitoalemdobolocombr'),
                'object_types'  => array('page'), // post type
                'show_on' => array('key' => 'page-template', 'value' => 'views/template-eventos.blade.php'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );

        //Gallery Images
        $cmb_gallery->add_field(
            array(
                'name' => __('Gallery images', 'muitoalemdobolocombr'),
                'id'   =>  $prefix . 'gallery_images',
                'type' => 'file_list',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text' => array(
                    'add_upload_files_text' => __('Add Images', 'muitoalemdobolocombr'),
                    'remove_image_text' => __('Remove Image', 'muitoalemdobolocombr'),
                    'file_text' => __('File', 'muitoalemdobolocombr'),
                    'file_download_text' => __('Download', 'muitoalemdobolocombr'),
                    'remove_text' => __('Remove', 'muitoalemdobolocombr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                        //  'image/gif',
                        'image/jpeg',
                        'image/png',
                    ),
                ),
                'preview_size' => array(320, 180)
            )
        );

        /**
         * Estimate
         */
        $cmb_estimate = new_cmb2_box(
            array(
                'id'            => 'muitoalemdobolocombr_events_estimate_id',
                'title'         => __('Estimate Form', 'muitoalemdobolocombr'),
                'object_types'  => array('page'), // post type
                'show_on' => array('key' => 'page-template', 'value' => 'views/template-eventos.blade.php'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );

        //Estimate Background Color
        $cmb_estimate->add_field(
            array(
                'name'       => __('Background Color', 'muitoalemdobolocombr'),
                'desc'       => '',
                'id'         => $prefix . 'estimate_background_color',
                'type'       => 'colorpicker',
                'attributes' => array(
                    'data-colorpicker' => json_encode(
                        array(
                            'palettes' => array( '#ffffff', '#e64461', '#fdd200','#fff9f3'),
                        )
                    ),
                ),
            )
        );
        
        //Estimate Background Image
        $cmb_estimate->add_field(
            array(
                'name'        => __('Background Image', 'muitoalemdobolocombr'),
                'description' => '',
                'id'          => $prefix . 'estimate_background_image',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'muitoalemdobolocombr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                        //'image/gif',
                        'image/jpeg',
                        'image/png',
                    ),
                ),
                'preview_size' => array(320, 180)
            )
        );

        //Estimate Title
        $cmb_estimate->add_field(
            array(
                'name'       => __('Title', 'muitoalemdobolocombr'),
                'desc'       => '',
                'id'         => $prefix . 'estimate_title',
                'type'       => 'textarea_code',
            )
        );

        //Estimate Form Shortcode
        $cmb_estimate->add_field(
            array(
                'name'       => __('Form Shortcode', 'muitoalemdobolocombr'),
                'desc'       => '',
                'id'         => $prefix . 'estimate_form_shortcode',
                'type'       => 'text',
            )
        );
    }
);

/**
 * Single Post (Blog)
 */
add_action(
    'cmb2_admin_init',
    function () {
        // Start with an underscore to hide fields from custom fields list
        $prefix = '_muitoalemdobolocombr_post_';

        /******
         * Hero
         ******/
        $cmb_hero = new_cmb2_box(
            array(
                'id'            => 'post_hero_id',
                'title'         => __('Hero', 'muitoalemdobolocombr'),
                'object_types'  => array('post'), // post type
                //'show_on'       =>  array('key' => 'page-template', 'value' => 'views/home.blade.php'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );

        //Hero Title
        $cmb_hero->add_field(
            array(
                'name'       => __('Title', 'muitoalemdobolocombr'),
                'desc'       => '',
                'id'         => $prefix . 'hero_title',
                'type'       => 'textarea_code',
                'default'    => 'Blog',
            )
        );
    }
);
